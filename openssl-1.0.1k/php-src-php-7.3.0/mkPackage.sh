#!/bin/bash

mkdir -p ~/php-lambda-layer/{bin,src}/
cp bootstrap.tmp ~/php-lambda-layer/bootstrap
cd ~/php-lambda-layer
chmod +x ./bootstrap
cp ~/php-7-bin/bin/php ./bin

curl -sS https://getcomposer.org/installer | ./bin/php
./bin/php composer.phar require guzzlehttp/guzzle
./bin/php composer.phar require aws/aws-sdk-php

zip -r runtime.zip bin bootstrap
zip -r vendor.zip vendor/

aws lambda publish-layer-version --layer-name php-runtime --zip-file fileb://runtime.zip --region ap-southeast-2
aws lambda publish-layer-version --layer-name php-vendor --zip-file fileb://vendor.zip --region ap-southeast-2
