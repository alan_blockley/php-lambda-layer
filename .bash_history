uname -a
sudo yum update -y
sudo yum install autoconf bison gcc gcc-c++ libcurl-devel libxml2-devel -y
curl -sL http://www.openssl.org/source/openssl-1.0.1k.tar.gz | tar -xvz
cd openssl-1.0.1k
./config && make && sudo make install
mkdir ~/php-7-bin
curl -sL https://github.com/php/php-src/archive/php-7.3.0.tar.gz | tar -xvz
cd php-src-php-7.3.0
./buildconf --force
./configure --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib
make install
/home/ec2-user/php-7-bin/bin/php -v
mkdir -p ~/php-example/{bin,src}/
cd ~/php-example
touch ./src/{hello,goodbye}.php
touch ./bootstrap && chmod +x ./bootstrap
cp ~/php-7-bin/bin/php ./bin
ls
nano bootstrap 
pwd
ls
curl -sS https://getcomposer.org/installer | ./bin/php
./bin/php composer.phar require guzzlehttp/guzzle
nano bootstrap 
nano src/hello.php 
ls
zip -r runtime.zip bin bootstrap
zip -r vendor.zip vendor/
zip hello.zip src/hello.php
zip goodbye.zip src/goodbye.php
ls
sudo pip install --upgrade awscli
nano /tmp/trust-policy.json
aws iam create-role     --role-name LambdaPhpExample     --path "/service-role/"     --assume-role-policy-document file:///tmp/trust-policy.json
aws lambda publish-layer-version     --layer-name php-example-runtime     --zip-file fileb://runtime.zip     --region ap-southeast-2
aws lambda publish-layer-version     --layer-name php-example-vendor     --zip-file fileb://vendor.zip --region ap-southeast-2
aws lambda create-function     --function-name php-example-hello     --handler hello     --zip-file fileb://./hello.zip     --runtime provided     --role "arn:aws:iam::784883214776:role/service-role/LambdaPhpExample"     --region us-east-1     --layers "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-runtime:1"           "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-vendor:1"
aws lambda create-function     --function-name php-example-goodbye     --handler goodbye     --zip-file fileb://./goodbye.zip     --runtime provided     --role "arn:aws:iam::784883214776:role/service-role/LambdaPhpExample"     --region us-east-1     --layers "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-runtime:1"           "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-vendor:1"
aws lambda create-function     --function-name php-example-hello     --handler hello     --zip-file fileb://./hello.zip     --runtime provided     --role "arn:aws:iam::784883214776:role/service-role/LambdaPhpExample"     --region ap-southeast-2     --layers "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-runtime:1"           "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-vendor:1"
aws lambda create-function     --function-name php-example-goodbye     --handler goodbye     --zip-file fileb://./goodbye.zip     --runtime provided     --role "arn:aws:iam::784883214776:role/service-role/LambdaPhpExample"     --region ap-southeast-2     --layers "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-runtime:1"           "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-vendor:1"
ls
nano src/goodbye.php 
zip goodbye.zip src/goodbye.php
aws lambda create-function     --function-name php-example-goodbye     --handler goodbye     --zip-file fileb://./goodbye.zip     --runtime provided     --role "arn:aws:iam::784883214776:role/service-role/LambdaPhpExample"     --region ap-southeast-2     --layers "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-runtime:1"           "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-vendor:1"
aws lambda invoke     --function-name php-example-hello     --region ap-southeast-2     --log-type Tail     --query 'LogResult'     --output text     --payload '{"name": "World"}' hello-output.txt | base64 --decode
aws lambda invoke     --function-name php-example-goodbye     --region ap-southeast-2     --log-type Tail     --query 'LogResult'     --output text     --payload '{"name": "World"}' goodbye-output.txt | base64 --decode
ls
nano src/hello.php 
zip hello.zip src/hello.php
aws lambda create-function     --function-name php-example-hello     --handler hello     --zip-file fileb://./hello.zip     --runtime provided     --role "arn:aws:iam::784883214776:role/service-role/LambdaPhpExample"     --region ap-southeast-2     --layers "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-runtime:1"           "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-vendor:1"
aws lambda invoke     --function-name php-example-hello     --region ap-southeast-2     --log-type Tail     --query 'LogResult'     --output text     --payload '{"name": "World"}' hello-output.txt | base64 --decode
ls
nano hello
nano hello-output.txt 
nano goodbye-output.txt 
ls
aws lambda create-function     --function-name php-example-hello     --handler hello     --zip-file fileb://./hello.zip     --runtime provided     --role "arn:aws:iam::784883214776:role/service-role/LambdaPhpExample"     --region ap-southeast-2     --layers "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-runtime:1"           "arn:aws:lambda:ap-southeast-2:784883214776:layer:php-example-vendor:1"
aws lambda invoke     --function-name php-example-hello     --region ap-southeast-2     --log-type Tail     --query 'LogResult'     --output text     --payload '{"name": "World"}' hello-output.txt | base64 --decode
aws lambda publish-layer-version     --layer-name php-runtime     --zip-file fileb://runtime.zip     --region ap-southeast-2
aws lambda publish-layer-version     --layer-name php-vendor     --zip-file fileb://vendor.zip --region ap-southeast-2
aws lambda invoke     --function-name php-example-hello     --region ap-southeast-2     --log-type Tail     --query 'LogResult'     --output text     --payload '{"name": "World"}' hello-output.txt | base64 --decode
ls
history 
ls
pwd
ls
pwd
cd ..
ls
tar cvfp php-example.tar php-example/
ls -la
history 
ls
cd php-
cd php-example
ls
nano hello
nano hello-output.txt 
ls
cd bin
ls
cd ..
ls
cd ..
ls
cd php-
cd php-7-bin/
ls
yum install     autoconf     automake     libtool     bison     re2c     libxml2-devel \
    libpng-devel     libjpeg-devel     curl-devel -y \
yum install     autoconf     automake     libtool     bison     re2c     libxml2-devel openssl-devel     libpng-devel     libjpeg-devel     curl-devel -y     mysql
sudo yum install     autoconf     automake     libtool     bison     re2c     libxml2-devel openssl-devel     libpng-devel     libjpeg-devel     curl-devel -y     mysql
cd openssl-1.0.1k
cd
cd openssl-1.0.1k
./config && make && sudo make install
history 
cd ..
ls
cd php-
cd php-7-bin/
ls
cd ..
ls
cd
ls
cd
ls
pwd
cd openssl-1.0.1k/
ls
cd p
cd php-src-php-7.3.0/
ls
cd ..
ls
mv php-src-php-7.3.0/ ../
ls
cd ../
ls
cd php-src-php-7.3.0/
ls
./buildconf --force
./configure     --enable-static=yes     --enable-shared=no     --disable-all     --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --with-openssl     --without-pear     --with-mysql     --with-mysqli     --with-pdo     --with-pdo-mysql
./configure     --enable-static=yes     --enable-shared=no     --disable-all     --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --with-openssl     --without-pear  --with-mysqli    --with-pdo-mysql
history 
./configure     --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-static=yes     --enable-shared=no     --disable-all     --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --with-openssl     --without-pear  --with-mysqli    --with-pdo-mysql
make install
pwd
ls
make clean
cd ..
ls
mv php-src-php-7.3.0/ openssl-1.0.1k/
cd openssl-1.0.1k/
ls
cd php-src-php-7.3.0/
./configure     --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-static=yes     --enable-shared=no     --disable-all     --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --with-openssl     --without-pear  --with-mysqli    --with-pdo-mysql
make clean
./buildconf --force
./configure     --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-static=yes     --enable-shared=no     --disable-all     --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --with-openssl     --without-pear  --with-mysqli    --with-pdo-mysql
make install
/home/ec2-user/php-7-bin/bin/php -v
ls
cd ..
ls
cd ..
ls
cd php-7-bin/
ls
cd bin
ls
cd ..
ls
cd ..
ls
history 
pwd
ls
cd php-example
ls
cd ..
ls
mv php-example php-example.v1
mkdir -p ~/php-example/{bin,src}/
cd ~/php-example
touch ./src/{hello,goodbye}.php
touch ./bootstrap && chmod +x ./bootstrap
cp ~/php-7-bin/bin/php ./bin
curl -sS https://getcomposer.org/installer | ./bin/php
./bin/php composer.phar require guzzlehttp/guzzle
ls
cd ..
ls
rm php-example -rf
ls
cd openssl-1.0.1k/
ls
cd php-src-php-7.3.0/
make clean
./buildconf --force
./configure     --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-static=yes     --enable-shared=no     --disable-all     --enable-hash        --enable-filter    --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --with-openssl     --without-pear  --with-mysqli    --with-pdo-mysql
make install
aws help
aws
aws secretmanager
aws secretsmanager
aws secretsmanager help
aws secretsmanager list-secrets
aws secretsmanager list-secrets --region ap-southeast-2
mkdir -p ~/php-example/{bin,src}/
cd ~/php-example
touch ./src/{hello,goodbye}.php
touch ./bootstrap && chmod +x ./bootstrap
cp ~/php-7-bin/bin/php ./bin
curl -sS https://getcomposer.org/installer | ./bin/php
./bin/php composer.phar require guzzlehttp/guzzle
composer require aws/aws-sdk-php
./bin/php composer.phar require aws/aws-sdk-php
cd ..ls
ls
cd ..
ls
rm php-example -rf
cd openssl-1.0.1k/
ls
cd php-src-php-7.3.0/
./configure     --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-static=yes     --enable-shared=no     --disable-all     --enable-hash        --enable-filter    --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --with-openssl     --without-pear  --with-mysqli    --with-pdo-mysql --enable-simplexml
make clean
./buildconf --force
./configure     --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-static=yes     --enable-shared=no     --disable-all     --enable-hash        --enable-filter    --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --with-openssl     --without-pear  --with-mysqli    --with-pdo-mysql --enable-simplexml
history 
make instal
make install
ls
mkdir -p ~/php-example/{bin,src}/
cd ~/php-example
touch ./src/{hello,goodbye}.php
touch ./bootstrap && chmod +x ./bootstrap
cp ~/php-7-bin/bin/php ./bin
curl -sS https://getcomposer.org/installer | ./bin/php
./bin/php composer.phar require guzzlehttp/guzzle
history 
composer require aws/aws-sdk-php
./bin/php composer.phar require aws/aws-sdk-php
ls
zip -r runtime.zip bin bootstrap
zip -r vendor.zip vendor/
./bin/php -m
ls
history 
history | grep vendor
history | grep public-layer-version
history | grep publish-layer-version
aws lambda publish-layer-version     --layer-name php-runtime     --zip-file fileb://runtime.zip     --region ap-southeast-2
aws lambda publish-layer-version     --layer-name php-vendor     --zip-file fileb://vendor.zip --region ap-southeast-2
ls
nano bootstrap 
history 
zip -r runtime.zip bin bootstrap
zip -r vendor.zip vendor/
aws lambda publish-layer-version     --layer-name php-runtime     --zip-file fileb://runtime.zip     --region ap-southeast-2
aws lambda publish-layer-version     --layer-name php-vendor     --zip-file fileb://vendor.zip --region ap-southeast-2
ls
ls bin/
history 
cd ..
ls
rm php-example
rm php-example -rf
cd openssl-1.0.1k/
ls
cd php-src-php-7.3.0/
ls
make clean
ls
./buildconf --force
history 
/configure     --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-static=yes     --enable-shared=no     --disable-all     --enable-hash        --enable-filter    --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --without-pear  --with-mysqli    --with-pdo-mysql --enable-simplexml
./configure     --prefix=/home/ec2-user/php-7-bin/ --with-openssl=/usr/local/ssl --with-curl --with-zlib --enable-static=yes     --enable-shared=no     --disable-all     --enable-hash        --enable-filter    --enable-json     --enable-libxml     --enable-mbstring     --enable-phar     --enable-soap     --enable-xml     --enable-pdo     --with-curl     --with-gd     --with-zlib     --without-pear  --with-mysqli    --with-pdo-mysql --enable-simplexml
make install
ls
nano mkPackage.sh
nano bootstrap.tmp
nano mkPackage.sh
nano mkPackage.sh 
history | grep publish
nano mkPackage.sh 
chmod +x mkPackage.sh 
cp mkPackage.sh ~/
./mkPackage.sh 
mysql -hdev-atlassian-01.ci0dyt7ivkkb.ap-southeast-2.rds.amazonaws.com -ujira.db -pd49cc532a3
ifconfig 
ls
cd php-lambda-layer/
ls
cd vendor
ls
cd aws/
ls
cd aws-sdk-php/
ls
cd src/
ls
cd CostExplorer/
ls
cd ..
ls
cd CloudWatch
ls
cd ..
ls
cd Exception/
ls
cd ..
ls
cd CostExplorer/
ls
nano CostExplorerClient.php 
cd Exception/
ls
nano CostExplorerException.php 
ls
ifconfig 
ls
du -sh 
nano mkPackage.sh 
pwd
ls
sudo tar cvfp ../php-lambda-layer.tar *
cd ..
ls
sudo gzip php-lambda-layer.tar 
ls -lah
ifconfig 
ls
tar zxvf /tmp/mysql-8.0.15.tar.gz 
ls
cd mysql-8.0.15/
ls
nano README 
nano INSTALL 
./configure
cat INSTALL 
find . -name 'mysqldump'
find . -name 'mysqldump*'
cd client/
ls
make
cmake ..
sudo yum install cmake
cmake ..
sudo yum install gcc g++ gcc gcc-c++ cmake
cd
wget https://dl.bintray.com/boostorg/release/1.65.1/source/boost_1_65_1.tar.gz
wget http://dlib.net/files/dlib-19.7.tar.bz2
ls
tar zxvf boost_1_65_1.tar.gz 
ls
tar xvf dlib-19.7.tar.bz2 
ls
mv boost_1_65_1.tar.gz /tmp
mv dlib-19.7.tar.bz2 /tmp
ls /tmp
ls
cd boost_1_65_1/
ls
./bootstrap.sh 
./bootstrap.sh -prefer=/usr
./bootstrap.sh -prefix=/usr
sudo ./b2 
export BOOST_INCLUDEDIR=/home/ec2-user/boost_1_65_1
sudo -E python3 setup.py install
export BOOST_ROOT=/home/ec2-user/boost_1_65_1
sudo -E python3 setup.py install
export BOOST_LIBRARYDIR=/home/ec2-user/boost_1_65_1/stage/lib
ls
cd .
cd ..
ls
cd mysql-8.0.15/
ls
cd client/
ls
cmake
cmake .
cmake . -DWITH_BOOST=/usr
cmake . -DDOWNLOAD_BOOST=1
yum install git
sudo yum install git
cmake . -DDOWNLOAD_BOOST=1
cmake . -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost
sudo yum install ncurses
sudo yum install ncurses-de
sudo yum install ncurses-dev
sudo yum install ncurses-devel
cmake . -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost
cmake . -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost -DWITH_CURSES=/usr/
cmake . -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost 
ls ~/boost
ls ~/boost/boost_1_68_0/
ls
cmake .
cmake 
sudo su - 
cmake . -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost -DWITH_CURSES=/usr/include/
cmake . -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost -DWITH_CURSES=/usr/include/ncurses
cmake . -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost -DWITH_NCURSES=/usr/include/ncurses
sudo yum install curses-devel
sudo yum install curses
cmake -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost -D CMAKE_PREFIX_PATH=/usr/include/ncurses .
cmake -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost -DCMAKE_PREFIX_PATH=/usr/include/ncurses .
cmake -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost -DCMAKE_PREFIX_PATH=/usr .
ls
cmake clean
rm CMakeCache.txt 
cmake -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost .
cmake .
ls
cmake ..
cmake .
cd ..
ls
cd ..
ls
rm mysql-8.0.15/ -rf
tar zxvf /tmp/mysql-8.0.15.tar.gz 
cd mysql-8.0.15/
ls
cd client/
cmake . 
cmake -DDOWNLOAD_BOOST=1 -DWITH_BOOST=~/boost .
cmake -DWITH_BOOST=~/boost .
cmake -Wno-dev -DWITH_BOOST=~/boost .
find . -name 'compile_flags.cmake'
find .. -name 'compile_flags.cmake'
cmake -Wno-dev -DWITH_BOOST=~/boost ..
cd ..
cmake -Wno-dev -DWITH_BOOST=~/boost ..
cmake -Wno-dev -DWITH_BOOST=~/boost .
mkdir bld
cd bld
cmake ..
ls
cd ..
ls
rm bld -rf
ls
cd ..
ls
mkdir mysql-build
cd mysql-
cd mysql-build/
cmake ../mysql-8.0.15/
cmake ../mysql-8.0.15/ -DFORCE_INSOURCE_BUILD=1
ls
make
cd ../mysql-8.0.15/
ls
make
ls
cd mysql-
ls
cd mysql-build/
ls
cd ..
cd mysql-8.0.15/
ls
cmake ../mysql-8.0.15/ -DFORCE_INSOURCE_BUILD=1
make
tmux
sudo yum install tmux
tmux
tmux attach
df -h
watch df /
ls
cd php-lambda-layer/
ls
cd bin
ls
cd ..
ls
cd ..
ls
cd php-lambda-layer/
ls
cp /usr/bin/mysql
ls
sudo yum remove mysql-server
mysql
sudo yum
sudo yum list
sudo yum list | grep mysql
ls
cd ..
ls
./mkPackage.sh 
ls
nano mkPackage.sh 
cd php-8
cd php-7-bin/
ls
cd etc/
ls
cd ..
ls
cd var
ls
cd ..
ls
cd ..
ls
cd php-lambda-layer/
ls
cat bootstrap 
ls
cd ..
find . -name 'bootstrap.tmp'
cd openssl-1.0.1k/
ls
cd php-src-php-7.3.0/
ls
nano bootstrap.tmp 
../../mkPackage.sh 
ls
nano ../../mkPackage.sh 
../../mkPackage.sh 
ls
cd ..
ls
cd ..
ls
cd php-lambda-layer/
ls
shutdown -h now
sudo shutdown -h now
ls
make
df -h
ls
cd ..
ls
du 0sh 
du -sh
ls
du -h --max-depth=1
rm mysql-build/ -rf
cd mysql-8.0.15/
ls
cd bin
s
ls
cd ..
ls
cd client/
ls
cd ..
rm mysql-8.0.15/ -rf
ls
rm boost_1_65_1/
rm boost_1_65_1/ -rf
sudo rm boost* -rf
ls
du -sh
df -h
sudo su - 
